package manifestacije;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {

	public static Scanner sc = new Scanner(System.in);
	public static Scanner scStr = new Scanner(System.in);

	public static void prikaziMeni() {
		System.out.println("**************************************************************************\r\n"
				+ "Izaberite opciju: \r\n"
				+ "**************************************************************************\r\n"
				+ "1)Prikaz gradova\r\n" + "2)Prikaz manifestacije\r\n" + "3)Pretraga manifestacije\r\n" + "4)Izmena naziva manifestacije\r\n" + "5)Sacuvati u fajl najvecu manifestaciju\r\n" + "6)Ocitaj i ispisi najvecu manifestaciju\r\n"
						+ "7)unos izmena brisanje manifestacije\r\n"
						+ "8)izmena brisanje grada + po id\r\n"
				+ "0)IZLAZ\r\n" + "**************************************************************************");
	}
	
	public static int ocitajCeoBroj(){
		while (sc.hasNextInt()==false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		int ceoBroj = sc.nextInt();
		sc.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teste posla broja
		return ceoBroj;
	}
	
	public static Manifestacije pretragaPoIdManifestacije(Statement stmt) throws SQLException {
		System.out.println("Unesite Id manifestacije");
		int broj = ocitajCeoBroj();
		String sql = "SELECT k.id_kulturna_manifestacija,k.naziv,k.broj_posetilaca,g.id_grad,g.naziv,g.ptt FROM manifestacije.kulturna_manifestacija as k INNER JOIN grad as g on g.id_grad = k.grad_id_grad WHERE id_kulturna_manifestacija = " + broj;
		ResultSet rset = stmt.executeQuery(sql);
		if(rset.next()) {
			Manifestacije manifestacija = new Manifestacije();
			manifestacija.setId(rset.getInt(1));
			manifestacija.setNaziv(rset.getString(2));
			manifestacija.setBrojPosetilaca(rset.getInt(3));
			Grad grad = new Grad();
			grad.setId(rset.getInt(4));
			grad.setNaziv(rset.getString(5));
			grad.setPtt(rset.getInt(6));
			manifestacija.setGrad(grad);
			return manifestacija;
		}
		return null;
		
	}
	
	
	public static List<Grad> ucitajGradove(Statement stmt) throws SQLException {
		String sql = "SELECT * FROM manifestacije.grad;";
		ResultSet rset =  stmt.executeQuery(sql);
		List<Grad> gradovi  = new ArrayList<>(); 
		while(rset.next()) {
			Grad grad= new Grad();
			grad.setId(rset.getInt(1));
			grad.setNaziv(rset.getString(2));
			grad.setPtt(rset.getInt(3));
			gradovi.add(grad);
		}
		try {
			rset.close();
		} catch (Exception ex1) {
			ex1.printStackTrace();
		}
		return gradovi;
	}
	
	public static void upisiManifestacijuUFile(Manifestacije manifestacija, File file) throws IOException {
		
			PrintWriter out2 = new PrintWriter(new FileWriter(file));
				out2.println(manifestacija.toFileRepresentation());
			
			
			out2.flush();
			out2.close();
	
		
		
	}
	
	public static String ocitajTekst(){
		String tekst = "";
		while(tekst == null || tekst.equals(""))
			tekst = sc.nextLine();
		
		return tekst;
	}
	
	
	
	static void citajIzFajlaManifestaciju(File file) throws IOException {
		if(file.exists()){

			BufferedReader in = new BufferedReader(new FileReader(file));
			
			in.mark(1); 
			if(in.read()!='\ufeff'){
				in.reset();
			}
			
			String s2;
			while((s2 = in.readLine()) != null) {
				System.out.println("Manifestacija sa najvise posetilaca je " + s2);
			}
			in.close();
		} else {
			System.out.println("Ne postoji fajl!");
		}
	}
	
	
	
	public static List<Manifestacije> ucitajManifestacije(Statement stmt) throws SQLException {
		String sql = "SELECT k.id_kulturna_manifestacija,k.naziv,k.broj_posetilaca,g.id_grad,g.naziv,g.ptt FROM manifestacije.kulturna_manifestacija as k INNER JOIN grad as g on g.id_grad = k.grad_id_grad";
		ResultSet rset =  stmt.executeQuery(sql);
		List<Manifestacije> manifestacije = new ArrayList<>();
		while(rset.next()) {
			Manifestacije manifestacija = new Manifestacije();
			manifestacija.setId(rset.getInt(1));
			manifestacija.setNaziv(rset.getString(2));
			manifestacija.setBrojPosetilaca(rset.getInt(3));
			Grad grad = new Grad();
			grad.setId(rset.getInt(4));
			grad.setNaziv(rset.getString(5));
			grad.setPtt(rset.getInt(6));
			manifestacija.setGrad(grad);
			manifestacije.add(manifestacija);
		}
		try {
			rset.close();
		} catch (Exception ex1) {
			ex1.printStackTrace();
		}
		return manifestacije;
	}
	
	public static void unosManifestacije(Statement stmt) throws SQLException {
		System.out.println("Izaberite grad");
		List<Grad> gradovi = ucitajGradove(stmt);
		gradovi.forEach(System.out::println);
		int idGrad = Test.ocitajCeoBroj();
		boolean postoji = false;
		for (Grad grad : gradovi) {
			if(grad.getId() == idGrad) {
				postoji = true;
				break;
			}	
		}
		if(!postoji) {
			System.out.println("Grad sa odabranim id-jem ne postoji!");
			return;
		}
		System.out.print("Unesi naziv:");
		String naziv = Test.ocitajTekst();
		System.out.print("Unesi broj posetilaca:");
		int brojPosetilaca = Test.ocitajCeoBroj();
		
		try {
			String sql = "INSERT INTO manifestacije.kulturna_manifestacija (grad_id_grad, naziv, broj_posetilaca) VALUES (?, ?, ?);";
			PreparedStatement pstmt = ConnectionManager.getConnection().prepareStatement(sql);
			
			pstmt.setInt(1, idGrad);
			pstmt.setString(2, naziv);
			pstmt.setInt(3, brojPosetilaca);
			
			pstmt.executeUpdate();
			System.out.println("Manifestacija uspesno dodata!");
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void izmenaManifestacije(Statement stmt) throws SQLException {
		System.out.println("Izaberite Manifestaciju");
		List<Manifestacije> manifestacije = ucitajManifestacije(stmt);
		manifestacije.forEach(System.out::println);
		int idManifestacija = Test.ocitajCeoBroj();
		boolean postoji = false;
		for (Manifestacije manifestacija : manifestacije) {
			if(manifestacija.getId() == idManifestacija) {
				postoji = true;
				break;
			}	
		}
		if(!postoji) {
			System.out.println("Manifestacija sa odabranim id-jem ne postoji!");
			return;
		}
		System.out.print("Unesi naziv:");
		String naziv = Test.ocitajTekst();
		System.out.print("Unesi broj posetilaca:");
		int brojPosetilaca = Test.ocitajCeoBroj();
		
		try {
			String sql = "UPDATE manifestacije.kulturna_manifestacija SET naziv = ?, broj_posetilaca = ? WHERE (id_kulturna_manifestacija = ?);";
			PreparedStatement pstmt = ConnectionManager.getConnection().prepareStatement(sql);
			
			
			pstmt.setString(1, naziv);
			pstmt.setInt(2, brojPosetilaca);
			pstmt.setInt(3, idManifestacija);
			
			pstmt.executeUpdate();
			System.out.println("Manifestacija uspesno promenjena!");
			
		}catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	private static void brisanjeManifestacije(Statement stmt) throws SQLException {
		
		System.out.println("Izaberite Manifestaciju");
		List<Manifestacije> manifestacije = ucitajManifestacije(stmt);
		manifestacije.forEach(System.out::println);
		int idManifestacija = Test.ocitajCeoBroj();
		boolean postoji = false;
		for (Manifestacije manifestacija : manifestacije) {
			if(manifestacija.getId() == idManifestacija) {
				postoji = true;
				break;
			}	
		}
		if(!postoji) {
			System.out.println("Manifestacija sa odabranim id-jem ne postoji!");
			return;
		}
		
		try {
			String sql = "DELETE FROM manifestacije.kulturna_manifestacija WHERE (id_kulturna_manifestacija = ?);";
			PreparedStatement pstmt = ConnectionManager.getConnection().prepareStatement(sql);
			pstmt.setInt(1, idManifestacija);
			pstmt.executeUpdate();
			System.out.println("Manifestacija uspesno obrisana");
			

			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}

	public static void main(String[] args) {
		File file = new File("./files/manifestacija.csv");
		try {
			// otvaranje konekcije, jednom na pocetku aplikacije
			ConnectionManager.open();
		} catch (Exception ex) {
			System.out.println("Neuspesna konekcija na bazu!");

			ex.printStackTrace();
			// kraj aplikacije
			return;
		}

		Statement stmt = null;

		try {
		int opcija = 1;
		stmt = ConnectionManager.getConnection().createStatement();
		do {
			try {
				prikaziMeni();
				opcija = sc.nextInt();
				switch (opcija) {

				case 1:
					List<Grad> gradovi = ucitajGradove(stmt);
					gradovi.forEach(System.out::println);

					break;
				case 2:
					List<Manifestacije> manifestacije = ucitajManifestacije(stmt);
					manifestacije.forEach(System.out::println);
					
					break;
				case 3:
					Manifestacije manifestacija = pretragaPoIdManifestacije(stmt);
					if(manifestacija == null){
						System.out.println("Manifestacija ne postoji!");
						break;
					}
					System.out.println(manifestacija);
					break;
				case 4:
					manifestacija = pretragaPoIdManifestacije(stmt);
					if(manifestacija == null){
						System.out.println("Manifestacija ne postoji!");
						break;
					}
					System.out.println("Izabrali ste manifestaciju: ");
					System.out.println(manifestacija);
					System.out.println("Unesite novi naziv ove manifestacije:");
					manifestacija.setNaziv(scStr.nextLine());
					String sql = "UPDATE manifestacije.kulturna_manifestacija SET naziv = '"+manifestacija.getNaziv()+"' WHERE (id_kulturna_manifestacija = "+manifestacija.getId()+");";
					stmt.executeUpdate(sql);
					System.out.println("Uspesno promenjen naziv");
					
					break;
				case 5:
					sql = "SELECT k.id_kulturna_manifestacija,k.naziv,k.broj_posetilaca,g.id_grad,g.naziv,g.ptt FROM manifestacije.kulturna_manifestacija as k INNER JOIN grad as g on g.id_grad = k.grad_id_grad order by broj_posetilaca DESC limit 1;";
					ResultSet rset = stmt.executeQuery(sql);
					Manifestacije najvecaManifestacija = new Manifestacije();
					if(rset.next()) {
					najvecaManifestacija.setId(rset.getInt(1));
					najvecaManifestacija.setNaziv(rset.getString(2));
					najvecaManifestacija.setBrojPosetilaca(rset.getInt(3));
					Grad grad = new Grad();
					grad.setId(rset.getInt(4));
					grad.setNaziv(rset.getString(5));
					grad.setPtt(rset.getInt(6));
					najvecaManifestacija.setGrad(grad);
					upisiManifestacijuUFile(najvecaManifestacija, file);
					System.out.println("Upesno upisano!");
					break;
					}
					System.out.println("Nema manifestacija!");
					break;
				case 6:
					citajIzFajlaManifestaciju(file);
					break;
					
				case 7:
					System.out.println("Izaberite:\n1)Unos manifestacije\n2)Izmena manifestacije\n3)Brisanje manifestacije");
					int odabir = sc.nextInt();
					if(odabir == 1) {
						unosManifestacije(stmt);
					} else if(odabir == 2) {
						izmenaManifestacije(stmt);
					} else if (odabir == 3) {
						brisanjeManifestacije(stmt);
					} else {
						System.out.println("Pogresan unos");
					}
					break;
				case 8:
					// izmena , brisanje grada
					break;
				case 0:
					System.out.println("IZLAZ");
					break;
				default:
					System.out.println("Pogresan unos!");

				}

			} catch (Exception e) {
				System.out.println("Pogresan unos!");
				sc = new Scanner(System.in);
				e.printStackTrace();
			}

		} while (opcija != 0);
		}catch(Exception e) {
			System.out.println("Greska u radu sa bazom!");
		}finally {
			
			try {
				stmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		

		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			ConnectionManager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	


	

}
