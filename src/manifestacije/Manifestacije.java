package manifestacije;

public class Manifestacije {
	
	private int id;
	private String naziv;
	private int brojPosetilaca;
	private Grad grad;
	
	
	
	
	public Manifestacije() {
		super();
	}
	public Manifestacije(int id, String naziv, int brojPosetilaca, Grad grad) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojPosetilaca = brojPosetilaca;
		this.grad = grad;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getBrojPosetilaca() {
		return brojPosetilaca;
	}
	public void setBrojPosetilaca(int brojPosetilaca) {
		this.brojPosetilaca = brojPosetilaca;
	}
	public Grad getGrad() {
		return grad;
	}
	public void setGrad(Grad grad) {
		this.grad = grad;
	}
	
	@Override
	public String toString() {
		return "Manifestacije [id=" + id + ", naziv=" + naziv + ", brojPosetilaca=" + brojPosetilaca + ", grad=" + grad
				+ "]";
	}
	
	public String toFileRepresentation(){
		
		StringBuilder bild = new StringBuilder(); 
		bild.append(naziv+","+brojPosetilaca);
		return bild.toString();
	}
	
	
	
	

}
